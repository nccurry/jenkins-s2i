FROM jenkins/jenkins:lts

USER root

RUN chown -R 1001:0 /usr/share/jenkins \
  && chown -R 1001:0 /var/jenkins_home